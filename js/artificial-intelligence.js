// 定义一个对象，包含问题和对应的回答
var knowledgeBase = {
  "你好": "你好，有什么我可以帮助你的吗？",
  "你是谁": "我是AI智能客服系统。",
  "天气怎么样": "你在哪个城市？",
  "北京": "北京的天气是晴朗的。",
  "上海": "上海的天气是多云的。",
  "谢谢": "不客气，有什么其他问题我可以帮助你的吗？"
};

// 定义一个函数，用于处理用户输入的问题
function processInput(input) {
  // 遍历知识库，查找匹配的问题
  for (var question in knowledgeBase) {
    if (input.includes(question)) {
      return knowledgeBase[question]; // 返回匹配的回答
    }
  }
  
  // 如果没有匹配的问题，返回默认回答
  return "抱歉，我不明白你的问题。";
}

// 测试代码
var userInput = "天气怎么样";
var answer = processInput(userInput);
console.log(answer); // 输出："你在哪个城市？"