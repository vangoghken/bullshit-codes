'''
坑，咱就要坑到底 [doge]

新线程->启动进程->启动线程->启动进程->启动线程->启动进程->启动线程->启动进程->启动线程->...
(主进程：(ZZZ~))
'''

import inspect
import datetime
import threading
import multiprocessing


def sub_thread():
    thread = threading.Thread(target=sub_process)
    thread.start()
    print(f'A thread is started in {inspect.currentframe().f_code.co_name} at {datetime.datetime.now().strftime("%H:%M:%S")}')    # 日志君
    thread.join()


def sub_process():
    process = multiprocessing.Process(target=sub_thread)
    process.start()
    print(f'Process (pid: {process.pid}) is started in {inspect.currentframe().f_code.co_name} at {datetime.datetime.now().strftime("%H:%M:%S")}')
    process.join()


def main():
    while True:
        thread = threading.Thread(target=sub_thread)
        thread.start()
        print(f'A thread is started in {inspect.currentframe().f_code.co_name} at {datetime.datetime.now().strftime("%H:%M:%S")}')
        thread.join()


if __name__ == '__main__':
    main()
