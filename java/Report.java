

package com.bimcloud.bim.work.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 报表管理
 */
@Data
@ApiModel(value = "报表管理")
public class Report {

	private String deptId;

	private String flag;
	/**
	 * 区域类型(1管控2专属)
	 */
	private String areaType;
	@ApiModelProperty(value = "年份")
	private int tjYear;

	@ApiModelProperty(value = "查询时间")
	private String tjTime;

	@ApiModelProperty(value = "月份")
	private String tjMonth;

	private int startMonth;
	private int endMonth;

	@ApiModelProperty(value = "开始时间")
	private String startTime;

	@ApiModelProperty(value = "结束时间")
	private String endTime;


	/**
	 * 工单来源 0自动上报1工程师上报2其它人员上报
	 */
	private int reportType0;
	private int reportType1;
	private int reportType2;
	/**
	 * 故障分布（字典：1设备离线2单灯离线3异常亮灯4异常关灯5市电掉电6漏电报警7柜门打开8缺项报警9其他）
	 */

	private int faultLevel1;
	private int faultLevel2;
	private int faultLevel3;
	private int faultLevel4;
	private int faultLevel5;
	private int faultLevel6;
	private int faultLevel7;
	private int faultLevel8;
	private int faultLevel9;

	//每月工单数
	private int[] monthTotal;


	//待分配工单、待处理工单、已完成工单数量
	private int taskStatus1;
	private int taskStatus3;
	private int taskStatus7;


	//巡视任务不同状态数量 0起草中1进行中2已结束
	private int tourStatus0;
	private int tourStatus1;
	private int tourStatus2;

	private int[] monthTourTotal;

	//巡视任务不同类型数量 0日常巡视1重点保障巡视2特殊巡视
	private int patrolType0;
	private int patrolType1;
	private int patrolType2;
	private int[] patrolTypeTotal;
	//不同类型的统计类型统计

	private int patrolTypeWxs0;
	private int patrolTypeEnd0;
	private int patrolTypeZ0;


	private int patrolTypeWxs1;
	private int patrolTypeEnd1;
	private int patrolTypeZ1;
	private int patrolTypeWxs2;
	private int patrolTypeEnd2;
	private int patrolTypeZ2;

   //进行中
	private int patrolType01;
	private int patrolType11;
	private int patrolType21;

	//起草中
	private int[] patrolStatusTotal0;
	//进行中
	private int[] patrolStatusTotal1;
	//已结束
	private int[] patrolStatusTotal2;

	private int taskTotal;
	private int tourTotal;
	//月份
	private int infoMonth;

	//出入库金额
	private BigDecimal kAmount;
	private BigDecimal rkAmount;
	private BigDecimal ckAmount;
	//年检费用
	private BigDecimal inspectionAmount;
	//保费
	private BigDecimal premiumAmount;
	//维护费用
	private BigDecimal serviceAmount;
	//加油费用
	private BigDecimal fuelAmount;
	//灯杆
	private List<DevLinght> lightList;
	/**
	 * 巡视数量
	 */
	private int tourTaskDetailCount;

	/**
	 * 工单数量
	 */
	private int taskInfoTotalNum;

	private List<Long> ids;

	private String userId;

	//区域id
	private Long areaId;
	private String subCode;
	private String areaName;
	//围栏
	private String pointsHx;
	private int subCodeLength;
	/**
	 * 树级别
	 */
	private int treeLeavel;

	private String name;
	/**
	 * 区域中心点
	 */
	private String centerPoint;

	private String[] months;

	private String lightId;

	private List<Report>reportList;

	@ApiModelProperty("年月日类型")
	private Integer type;
}
