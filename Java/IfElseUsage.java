			// 业务代码
			Update update;
            if (operationType.equals(AuditResultOperationType.getStart())) {
                update = new Update().set("XXX", AuditResultOperationType.getStart());
            } else if (operationType.equals(AuditResultOperationType.getStopBystart())) {
                update = new Update().set("XXX", AuditResultOperationType.getStopBystart());
            } else if (operationType.equals(AuditResultOperationType.getImExe())) {
                update = new Update().set("XXX", AuditResultOperationType.getImExe());
            } else if (operationType.equals(AuditResultOperationType.getStopByimExe())) {
                update = new Update().set("XXX", AuditResultOperationType.getStopByimExe());
            } else {  //删除
                update = new Update().set("XXX", AuditResultOperationType.getDel());
            }
// 这是类....			
public class AuditResultOperationType {
    public static final String start = "启用";
    public static final String stopBystart = "停用";
    public static final String imExe = "立即执行";
    public static final String stopByimExe = "停止";
    public static final String del = "删除";

    public static Integer getStart() {
        return  0;
    }

    public static Integer getStopBystart() {
        return 1;
    }

    public static Integer getImExe() {
        return 2;
    }

    public static Integer getStopByimExe() {
        return 3;
    }

    public static Integer getDel() {
        return 4;
    }


}